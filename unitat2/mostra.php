<?php
$url=$_REQUEST["url"]; 

$xslDoc = new DOMDocument();
$xslDoc->load("llista_noticies.xsl");

$xmlDoc = new DOMDocument();
$xmlDoc->load($url);

$xsltProcessor = new XSLTProcessor();
$xsltProcessor->registerPHPFunctions();
$xsltProcessor->importStyleSheet($xslDoc);

foreach ($params as $key => $val)
$xsltProcessor->setParameter('', $key, $val);

echo $xsltProcessor->transformToXML($xmlDoc);
?>