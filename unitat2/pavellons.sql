-- phpMyAdmin SQL Dump
-- version 4.2.9.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 15-10-2014 a les 19:52:02
-- Versió del servidor: 5.5.8
-- PHP Version: 5.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `taller_xml`
--

-- --------------------------------------------------------

--
-- Estructura de la taula `pavellons`
--

CREATE TABLE IF NOT EXISTS `pavellons` (
`id` int(2) NOT NULL,
  `club` varchar(33) DEFAULT NULL,
  `pavello` varchar(50) DEFAULT NULL,
  `ciutat` varchar(50) DEFAULT NULL,
  `provincia` varchar(50) DEFAULT NULL,
  `coordenades` varchar(50) DEFAULT NULL,
  `descripcio` text
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

--
-- Bolcant dades de la taula `pavellons`
--

INSERT INTO `pavellons` (`id`, `club`, `pavello`, `ciutat`, `provincia`, `coordenades`, `descripcio`) VALUES
(1, '  Baloncesto Sevilla', '', '', '', '', ''),
(2, '  Bilbao Basket', '', '', '', '', ''),
(3, '  CAI Zaragoza', '', '', '', '', ''),
(4, '  FC Barcelona', 'Palau Blaugrana', 'Barcelona', 'Barcelona', ' 41.380018,   2.120052 ', '<a href="http://www.fcbarcelona.com/basketball"><img src=http://media1.fcbarcelona.com/media/asset_publics/resources/000/069/392/size_320x250/1314_BAR_equip__0_copia.v1384185029.jpg"></a>'),
(5, '  FIATC Joventut', '', '', '', '', ''),
(6, '  Gipuzkoa Basket', '', '', '', '', ''),
(7, '  Herbalife Gran Canaria', '', '', '', '', ''),
(8, '  Iberostar Tenerife', '', '', '', '', ''),
(9, '  Laboral Kutxa Baskonia', '', '', '', '', ''),
(10, '  La Bruixa d''Or Manresa', 'El Nou Congost', 'Manresa', 'Barcfelona', ' 41.725350 ,1.809582', 'Partit de lliga ACB: <br>\r\n<a href="http://www.basquetmanresa.com/?lang=es">\r\n<img src="http://www.basquetmanresa.com/wp-content/uploads/2014/10/MG_9629-620x330.jpg"> </a>'),
(11, '  Montakit Fuenlabrada', '', '', '', '', ''),
(12, '  MoraBanc Andorra', '', '', '', '', ''),
(13, '  Real Madrid', '', '', '', '', ''),
(14, '  Rio Natura Monbus Obradoiro', '', '', '', '', ''),
(15, '  Tuenti Móvil Estudiantes', '', '', '', '', ''),
(16, '  Unicaja', '', '', '', '', ''),
(17, '  Universidad Católica de Murcia', '', '', '', '', ''),
(18, '  Valencia Basket', '', '', '', '', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pavellons`
--
ALTER TABLE `pavellons`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pavellons`
--
ALTER TABLE `pavellons`
MODIFY `id` int(2) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
