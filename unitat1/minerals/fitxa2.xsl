<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
<html lang="en">
<head>
  <meta charset="UTF-8"/>
  <style>

.taula1 {
	width: 90% ; 
	height: 39px; 
	border: 1; 
	background: #b8cbfa; 
	border: 1px solid #000;
	font-family: 'Arial,Helvetica' ; 
	font-weight: bold;

}  	

.logo{
	width: 60px; 
	height: 60px; 
	}
TD {
	 border: 1px solid #000;
}


  </style>

  </head>
<body>
<xsl:for-each select="minerals/mineral[@id='2']">
<P><TABLE class="taula1" >
<TR class="fila">
<TD >  &#160; <span ><xsl:value-of select="nom" /></span></TD>
<TD >  <span><xsl:value-of select="nom"/></span></TD>
<TD >  <span><xsl:value-of select="composicio"/></span></TD>
<TD >  &#160;  <span>Grup:</span><span><xsl:value-of select="grup"/></span></TD></TR>
</TABLE>
</P>
  <P><TABLE class="taula1">
<TR>
<TD>  <span>Pes espec&#237;fic: </span> <span><xsl:value-of select="densitat"/></span></TD>
<TD ROWSPAN='9'>  <P ALIGN="CENTER">
<img>
<xsl:attribute name="src">
<xsl:value-of select="foto" />
</xsl:attribute> 
</img> 
<br /><br /><br /><SMALL> <xsl:text>---</xsl:text></SMALL>
</P></TD></TR>
<TR>
<TD>  <span>Duresa: </span><span><xsl:value-of select="duressa"/></span></TD></TR>
<TR>
<TD>  <span>Exfoliaci&#243;: </span> <span><xsl:text>---</xsl:text></span></TD></TR>
<TR>
<TD >  <span>Color: </span><span><xsl:value-of select="color"/></span><span>Ratlla </span><span> <xsl:text>---</xsl:text> </span></TD></TR>
<TR>
<TD >  <span>Llu&#237;ssor: </span><span><xsl:text>---</xsl:text></span></TD></TR>
<TR>
<TD ROWSPAN='3'>  <span>Cristal.litzaci&#243;: </span><span><xsl:text>---</xsl:text></span></TD></TR>
<TR></TR>
<TR></TR>
<TR>
<TD>  <span>Aspecte: </span><span><xsl:text>---</xsl:text></span></TD></TR>
</TABLE>
</P>
  <P><TABLE class="taula1">
<TR>
<TD>  <span>Carater&#237;stiques generals: </span> <span><xsl:text>---</xsl:text></span></TD></TR>
<TR>
<TD>  <span>Jaciments: </span> <span><xsl:text>---</xsl:text></span></TD></TR>
<TR>
<TD>  <span>Utilitats i aplicacions: </span> <span><xsl:text>---</xsl:text></span></TD></TR>
<TR><TD><IMG class="logo" SRC='http://www.xtec.cat/~fmas/mine/logo.gif' /> &#160; &#160; &#160; <span>Fitxa original del Projecte </span></TD></TR>
</TABLE>
</P>
</xsl:for-each>
</body>
</html>
  </xsl:template>
</xsl:stylesheet>




