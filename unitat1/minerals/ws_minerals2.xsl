<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
  <html>
  <head>
  </head>
  <body>
  <h2>Mineral</h2>
  <xsl:for-each select="Taller_XML/minerals[Id='40']">
 <p><table border="1" width="100%" BGCOLOR='#b9cbf9' >
  <tr>
  <TD HEIGHT='36'>  <B> <xsl:value-of select="nom"/></B></TD>
  <TD>  <xsl:value-of select="nom"/></TD>
  <TD>  <xsl:value-of select="composicio" /></TD>
  <TD>  <B>Grup: </B> <xsl:value-of select="grup"/></TD>
  </tr>
  </table></p>
  <p>
  <table border="1" BGCOLOR='#b9cbf9' width='100%' >
<tr>
<TD BGCOLOR='#b9cbf9'>  <B> Pes espec�fic: </B><xsl:value-of select="densitat"/></TD>
<TD ROWSPAN='9'>  <P ALIGN="CENTER">
<img>
    <xsl:attribute name="src">
      <xsl:value-of select="foto" />
    </xsl:attribute> 
  </img>
  </P>
</TD>
</tr>
<tr>
<TD>  <B>Duresa: </B> <xsl:value-of select="duresa" /></TD>
</tr>
<tr>
<TD>  <B>Exfoliaci�: </B><xsl:value-of select="exfoliacio"/></TD>
</tr>
<tr>
<TD>   <B>Color: </B><xsl:value-of select="color"/><B> Ratlla: </B><xsl:value-of select="ratlla"/> </TD>
</tr>
<tr>
<TD>  <B>Llu�ssor: </B><xsl:value-of select="lluisor" /></TD>
</tr>
  </table></p>
 </xsl:for-each>
  </body>
  </html>
</xsl:template>
</xsl:stylesheet> 
