<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
<html lang="en">
<head>
  <meta charset="UTF-8"/>
  </head>
<body>
<xsl:for-each select="minerals/mineral[@id='2']">
<P><TABLE WIDTH='90%' BORDER='1' CELLPADDING='2' CELLSPACING='0' HEIGHT=
  '39'>
<TR>
<TD BGCOLOR='#b8cbfa' HEIGHT='36'>  &#160; <B><FONT FACE='Arial,Helvetica'><xsl:value-of select="nom" /></FONT></B></TD>
<TD BGCOLOR='#b9cbf9'>  <FONT FACE='Arial,Helvetica'><xsl:value-of select="nom"/></FONT></TD>
<TD BGCOLOR='#b9cbf9'>  <FONT FACE='Arial,Helvetica'><xsl:value-of select="composicio"/></FONT></TD>
<TD BGCOLOR='#b8cbfa'>  &#160;  <B><FONT FACE='Arial,Helvetica'>Grup:</FONT></B><FONT FACE='Arial,Helvetica'><xsl:value-of select="grup"/></FONT></TD></TR>
</TABLE>
</P>
  <P><TABLE WIDTH='90%' BORDER='1' BGCOLOR='#d0ddfb' CELLPADDING='4' CELLSPACING=
  '0'>
<TR>
<TD BGCOLOR='#b9cbf9'>  <B><FONT FACE='Arial,Helvetica'>Pes espec&#237;fic: </FONT> </B><xsl:value-of select="densitat"/></TD>
<TD ROWSPAN='9'>  <P ALIGN="CENTER">

<img>
<xsl:attribute name="src">
<xsl:value-of select="foto" />
</xsl:attribute> 
</img> 
<br /><br /><br /><SMALL> <xsl:text>---</xsl:text></SMALL>
</P></TD></TR>

<TR>
<TD BGCOLOR='#b9cbf9'>  <B><FONT FACE='Arial,Helvetica'>Duresa: </FONT></B><xsl:value-of select="duressa"/></TD></TR>
<TR>
<TD BGCOLOR='#b9cbf9'>  <B><FONT FACE='Arial,Helvetica'>Exfoliaci&#243;: </FONT> </B><xsl:text>---</xsl:text></TD></TR>
<TR>
<TD BGCOLOR='#b9cbf9'>  <B><FONT FACE='Arial,Helvetica'>Color: </FONT></B><xsl:value-of select="color"/>
   <B> Ratlla </B> <xsl:text>---</xsl:text> </TD></TR>
<TR>
<TD BGCOLOR='#b9cbf9'>  <B><FONT FACE='Arial,Helvetica'>Llu&#237;ssor: </FONT></B><xsl:text>---</xsl:text></TD></TR>
<TR>
<TD ROWSPAN='3' BGCOLOR='#b9cbf9'>  <B><FONT FACE='Arial,Helvetica'>Cristal.litzaci&#243;: </FONT> </B><xsl:text>---</xsl:text></TD></TR>
<TR></TR>
<TR></TR>
<TR>
<TD WIDTH='50%' BGCOLOR='#b9cbf9'>  <B><FONT FACE='Arial,Helvetica'>Aspecte: </FONT></B><xsl:text>---</xsl:text></TD></TR>
</TABLE>
</P>
  <P><TABLE WIDTH='90%' BGCOLOR='#d0ddfb' BORDER='1' CELLSPACING='0' CELLPADDING=
  '4' HEIGHT='0'>
<TR>
<TD BGCOLOR='#b8cbfa' WIDTH='90%'>  <B><FONT FACE='Arial,Helvetica'>Carater&#237;stiques generals: </FONT> </B><xsl:text>---</xsl:text></TD></TR>
<TR>
<TD BGCOLOR='#b8cbfa' WIDTH='90%'>  <B><FONT FACE='Arial,Helvetica'>Jaciments: </FONT> </B><xsl:text>---</xsl:text></TD></TR>
<TR>
<TD BGCOLOR='#b8cbfa' WIDTH='90%'>  <B><FONT FACE='Arial,Helvetica'>Utilitats i aplicacions: </FONT> </B><xsl:text>---</xsl:text></TD></TR>
<TR><TD BGCOLOR='#b8cbfa' WIDTH='90%'><IMG SRC='http://www.xtec.cat/~fmas/mine/logo.gif' width="30" heigth="30" /> &#160; &#160; &#160; <B>Fitxa original del Projecte </B></TD></TR>
</TABLE>
</P>
</xsl:for-each>
</body>
</html>
  </xsl:template>
</xsl:stylesheet>




